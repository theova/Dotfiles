###############################
# OH MY ZSH CONFIGURATION
#
###############################


export TERM=xterm-256color

# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/home/theo/.oh-my-zsh

###############################
# THEME, COLORS
###############################
# Set name of the theme to load.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="powerlevel9k/powerlevel9k"
DEFAULT_USER=theo
POWERLEVEL9K_MODE='awesome-fontconfig' # use FontAwesome
POWERLEVEL9K_TIME_FORMAT='%D{%H:%M}'
POWERLEVEL9K_VI_INSERT_MODE_STRING=""
POWERLEVEL9K_VI_COMMAND_MODE_STRING=""
POWERLEVEL9K_STATUS_CROSS=true
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir_writable dir rbenv vcs vi_mode)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator background_jobs time)
POWERLEVEL9K_SHORTEN_DIR_LENGTH=2
POWERLEVEL9K_SHORTEN_DELIMITER="."
POWERLEVEL9K_SHORTEN_STRATEGY='truncate_to_first_and_last'

# Which plugins would you like to load? (plugins can be found in $ZSH/plugins/*)
# Custom plugins may be added to $ZSH/custom/plugins/
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  colored-man-pages
  command-not-found
  compleat
  gpg-agent
  history
  pass
  per-directory-history
  vi-mode
  wd
)
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source $ZSH/oh-my-zsh.sh

# Bind autosuggest-execute to Shift+TAB. Find out keystring with
# sudo showkey --scancodes
bindkey '^@' autosuggest-execute
bindkey '^[M' autosuggest-execute
# SHIFT+TAB: navigate backwards through list
bindkey -M menuselect '^[[Z' reverse-menu-complete

# User configuration
# defaults
export VISUAL='nvim'
export TERM='termite'
export USER='theo'
export EDITOR='nvim'
export PAGER='less'

# Lanuages, locals
export LANG=de_DE.UTF-8
export LC_MESSAGES=de_DE.UTF-8
export LC_ALL=de_DE.UTF-8

# JAVA HOME
export INSTALL4J_JAVA_HOME=/opt/

# LaTeX user defined packages, documents, ...
export TEXMFHOME=~/.local/share/texmf/
# load aliases
source $ZSH_CUSTOM/aliases
