# My personal GNU/Linux

In this repository you can see my settings aka Dotfiles. I'm using [Arch Linux](https://www.archlinux.org/), but most of the tools should work with every GNU/Linux distribution. If you like the way I use my system and you are interested in learning more about GNU/Linux, consider moving to Arch.

To get started with Dotfiles, see [this article](https://www.anishathalye.com/2014/08/03/managing-your-dotfiles/). In short, it's not a good idea to copy everything into your home directory. This won't work! It's highly recommended to selectively copy what you need and to try to understand, what it means.

If you have any questions, feel free to ask. It would be nice to see your config as well!

## Software
In the following, you see a list with software I use. You can find the belonging dotfiles (if they are important) in `.config/`. Sometimes, there are `README.md` with further informations.

### Appearence/"Desktop environment"
* [i3-gapps](https://github.com/Airblader/i3): Window manager (vim-like)
* [Rofi](https://github.com/DaveDavenport/rof): application launcher
* [base16-manager](https://github.com/AuditeMarlow/base16-manager): set themes/colors for almost all my applications.
	* [gruvbox-dark-hard](https://github.com/dawikur/base16-gruvbox-scheme): used color scheme
* [Polybar](https://github.com/jaagr/polybar): Nice and easy-to-use status bar.
* [Compton](https://github.com/chjj/compton): Standalone compositor for window-effects.
* [termite](https://github.com/thestinger/termite/): keyboard centric terminal emulator.
* [unclutter](https://github.com/Airblader/unclutter-xfixes): Hide cursor if you don't need it.
* [redshift](http://jonls.dk/redshift/): Adjust color temperature in the evening.
* [feh](https://feh.finalrewind.org/): set the background.
	* Wallpaper by [Ian Schneider](https://unsplash.com/photos/PAykYb-8Er8).
* fonts:
	* [Hack](https://sourcefoundry.org/hack/): "A workhorse for code. No frills. No gimmicks."
	* [FontAwesome](https://fontawesome.com/): Icon set.

### The heart
* [neovim](https://neovim.io/): Keyboard centric editor, extremely extensible.
* [zsh](https://www.zsh.org/): Shell language, replacement for bash.
	* [Oh My ZSH](http://ohmyz.sh/): Framework, plugins.
	* [Powerlevel9k](https://github.com/bhilburn/powerlevel9k): Powerline theme

### Internet
* [qutebrowser](https://www.qutebrowser.org/): Browser (vim-like)
* [neomutt](https://github.com/neomutt/neomutt): Mail reader in the terminal (vim-like)
	* [offlineimap](https://offlineimap.org): Fetch your mails from the server
	* [msmtp](https://msmtp.sourceforge.net) Send your mails.
	* [notmuch](https://notmuchmail.org): query your mails.
	* [abook](http://abook.sourceforge.net/): the address book
* [Telegram](https://desktop.telegram.org/): messenger

### Media
* [whipper](https://github.com/JoeLametta/whipper): commandline cd import
* [beet](http://beets.io/): commandline media library management
* [cmus](https://cmus.github.io/): commandline music player (vim-like)
* [vimiv](http://karlch.github.io/vimiv/): image viewer (vim-like)

### Office
* [LaTeX](https://www.latex-project.org/): typesetting system
* [Zathura](https://www.pwmt.org/projects/zathura/): minimalistic, but full-featured PDF viewer (vim-like)
* [LibreOffice](https://www.libreoffice.org/): office suite

### Scientific
* [GNU Octave](https://www.gnu.org/software/octave/): Free matlab alternative

### Utilities:
* [ranger](http://ranger.github.io/): File manager (vim-like)
* [vifm](https://vifm.info/): File manager, use for work in two folders
* [pass](https://www.passwordstore.org/): Password management. Commandline interface
* [Deepin Screenshot](https://duckduckgo.com/?q=deepin+screenshot&ia=software): Screenshots, allows to manipulate them directly.
* [BorgBackup](https://www.borgbackup.org/): incremental backup, commandline interface
* [trash](https://github.com/andreafrancia/trash-cli): Commandline trash, instead of `rm`
* [udiskie](https://www.freedesktop.org/wiki/Software/udisks/): Automatic mount of external media, e.g. usb sticks.

## Screenshots

**Rofi**

![rofi](https://gitlab.com/theova/Dotfiles/raw/master/screenshots/rofi.png "rofi")

**Ranger and Neovim**

![ranger-neovim](https://gitlab.com/theova/Dotfiles/raw/master/screenshots/ranger-neovim.png "Screenshot ranger and neovim")

**qutebrowser and zsh/termite**

![qutebrowser-zsh](https://gitlab.com/theova/Dotfiles/raw/master/screenshots/qutebrowser-zsh.png "Screenshot qutebrowser and zsh")

**Neovim and colored man page**

![neovim-man](https://gitlab.com/theova/Dotfiles/raw/master/screenshots/neovim-man.png "nvim and man")

**Vimtex (nvim) and Zathura**

![vimtex-zathura](https://gitlab.com/theova/Dotfiles/raw/master/screenshots/vimtex-zathura.png "vimtex-zathura")

**Neomutt**

![neomutt](https://gitlab.com/theova/Dotfiles/raw/master/screenshots/neomutt.png "Screenshot neomutt")

**cmus**

![cmus](https://gitlab.com/theova/Dotfiles/raw/master/screenshots/cmus.png "Screenshot cmus")
