" set spellcheck
setlocal spell
setlocal spelllang=en,de

" remove all files except from *.pdf and .tex
autocmd VimLeave * VimtexClean

" wrap text automatically
setlocal wrap


" tex_conceal:
" If your encoding is utf-8, then a number of character sequences can be
" translated into appropriate utf-8 glyphs, including various accented
" characters, Greek characters in MathZones, and superscripts and subscripts
" in MathZones.
" Only conceal in normal mode
augroup concealtoggle
	autocmd!
	autocmd InsertLeave,BufEnter * set conceallevel=2 concealcursor=n
	autocmd InsertEnter * set conceallevel=0
augroup END

" completion with deoplete (see separate file completion.vim)
if !exists('g:deoplete#omni#input_patterns')
    let g:deoplete#omni#input_patterns = {}
endif
let g:deoplete#omni#input_patterns.tex = g:vimtex#re#deoplete
