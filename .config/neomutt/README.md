# Neomutt and it's friends
Neomutt is the "vim of mails", efficient, configurable and fast!
Neomutt follows the  [unix-philosophy](https://en.wikipedia.org/wiki/Unix_philosophy):
* Write programs that do one thing and do it well.
* Write programs to work together.
* Write programs to handle text streams, because that is a universal interface.

According to this, I use neomutt only to "handle" mails, e.g. overview, read mails, list attachments, .... 
Other jobs are done by other programs:
* fetch mails: offlineimap
* send mails: msmtp
* write mails: neovim
* open attachements: mailcap ([config](https://gitlab.com/theova/Dotfiles/blob/master/.mailcap))
* list urls: UrlView
* Addressbook: abook
* query mails: notmuch
* notification in stauts bar: [script](https://gitlab.com/theova/Dotfiles/blob/master/.config/polybar/notify-mail.sh) for polybar
* [lynx](http://lynx.invisible-island.net/): view html

## Screenshots
![Screenshot neomutt](https://gitlab.com/theova/Dotfiles/raw/master/screenshots/neomutt.png "Screenshot neomutt")

## Get started:
* [How to setup mutt](https://linux-club.de/wiki/opensuse/Mutt)
* [General Instructions](https://wiki.archlinux.org/index.php/Mutt)

## Further Config
### offlineimap
* [Password management with Gnome keyring](https://wiki.archlinux.org/index.php/Offlineimap#Gnome_keyring)
* [Running offlineimap in the background](https://wiki.archlinux.org/index.php/Offlineimap#Running_offlineimap_in_the_background)

### msmtp
* [Password management with Gnome keyring](https://wiki.archlinux.org/index.php/Offlineimap#Running_offlineimap_in_the_background)

### neovim
* [Emailing (mutt) and vim advanced config)](http://www.mdlerch.com/emailing-mutt-and-vim-advanced-config.html)
