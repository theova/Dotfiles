# Polybar
I use Polybar as my status bar. Polybar has a nice look, is easy to customize and works well with i3.

## Screenshots
![Screenshot polybar](https://gitlab.com/theova/Dotfiles/raw/master/screenshots/polybar.png "Screenshot Polybar")
## Additional requirements
* [FontAwesome](https://fontawesome.com/): Icon set.
