#!/bin/sh
maildir="$1"
if [ ! -d "${maildir}" ] ; then
	echo "$(date) - $maildir does not exist"
elif [ -n "$(ls "${maildir}")" ] ; then
	echo " "
else
	echo "" # is necessary to "clean" the output
fi

