#!/bin/sh

# The USB symbol from ttf-font-awesome
sym=""
# The default directory
dir="/media/"
num=0

if [ $# -ne 0 ]; then
	dir="${1%/}"
fi

if [ -d "$dir" ]; then
	for f in "$dir/"*; do
		# Outputs the size of the filesystem. 'avail' outputs available space,
		# it can be changed for 'size', 'used'... See man df for more information.
		if [ -f "$f" ]; then
			size=$(df --output=avail -h "$f" | tail -1)
			size="${size:1}B"
			res="$res$sym $f ($size)  "
			num=$num+1;
		fi
	done

	if [ "$(ls -1 "$dir" |wc -l)" -gt 0 ]; then
		echo "$sym"
	fi
else
	echo""
fi
