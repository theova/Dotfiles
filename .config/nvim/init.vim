"==========================================================================
" init.vim (settings for neovim)
"==========================================================================

" -------------------------------------------------------------------------
"  runtimepath
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
" -------------------------------------------------------------------------

" Setup Plug (plugin manager)
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source ~/.vimrc
endif

call plug#begin('~/.vim/plugged')
" -------------------------------------------------------------------------

" -------------------------------------------------------------------------
" Allgemeine Einstellungen
" -------------------------------------------------------------------------

" " unabhängig von Dateiname, Plugins, etc.
 source ~/.config/nvim/general.vim

" - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
" Installiere allgemeine Plugins
Plug 'kana/vim-textobj-user'		"benötigt für 'vim-textobj-entire'
Plug 'kana/vim-textobj-entire' 	"ie und ae für gesamten Textkörper
Plug 'tpope/vim-commentary'		" auskommentieren mit gc ...
Plug 'tpope/vim-surround'             " Textkörper umgeben mit Zeichen
Plug 'tpope/vim-repeat'               " Pluginbefehle mit '.'  wiederholen
" - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

"--------------------------------------------------------------------------
" Appearance
"--------------------------------------------------------------------------

" Colors:
Plug 'chriskempson/base16-vim'

" Statusline:
source ~/.config/nvim/statusline.vim

" Startscreen:
source ~/.config/nvim/startify.vim

"--------------------------------------------------------------------------
" Coding, generally
"--------------------------------------------------------------------------

" Syntaxchecking mit ALE
Plug 'w0rp/ale'
let g:ale_completion_enabled = 1
let g:ale_linters = {'tex': []} " disable ale for filetype tex
" Disable ale for PKGBUILDS
autocmd BufEnter PKGBUILD,.env
\   let b:ale_sh_shellcheck_exclusions = 'SC2034,SC2154,SC2164'
autocmd BufEnter config.py
\   let b:ale_python_mypy_ignore_invalid_syntax = 'F821'


" Completitition
source ~/.config/nvim/completition.vim

" tags
Plug 'ludovicchabant/vim-gutentags'
let g:gutentags_cache_dir = '~/.local/share/nvim/tags/' "store tags in this folder

" Git wrapper
Plug 'tpope/vim-fugitive'

" Project specific configurations:
Plug 'editorconfig/editorconfig-vim'
"compatibility with fugitive
let g:EditorConfig_exclude_patterns = ['fugitive://.*']
"--------------------------------------------------------------------------
" Language specific
"--------------------------------------------------------------------------

" LaTex mit Vimtex
source ~/.config/nvim/latex.vim

" Bash IDE
Plug 'WolfgangMehner/bash-support'

" GNU Octave (Matlab alternative)
Plug 'jvirtanen/vim-octave'

" Python:
Plug 'zchee/deoplete-jedi'
"--------------------------------------------------------------------------

" end for Plug
call plug#end()

" set colors
set termguicolors
source ~/.config/nvim/colorscheme.vim

"==========================================================================
" End of init.vim
"==========================================================================
