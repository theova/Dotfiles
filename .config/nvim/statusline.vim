" Installation with plugin manager
Plug 'vim-airline/vim-airline'

let g:airline#extensions#whitespace#checks = [ 'indent',  'long', 'mixed-indent-file' ]
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#show_close_button = 0
