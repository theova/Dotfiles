" VimTex, used for compile, etc...
Plug 'lervag/vimtex'

"" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
"" 'plaintex' instead of 'tex', which results in vimtex not being loaded.
"" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'

" Use Zathura as PDF-viewer (Zathura supports inverse searching)
let g:vimtex_view_method = 'zathura'

" necessary for nvim, see vimtex FAQ 'nvim'
let g:vimtex_compiler_progname = 'nvr'

let g:vimtex_complete_close_braces=1

" Customize which warnings are shown
let g:vimtex_quickfix_latexlog = {
			\ 'default' : 1,
			\ 'general' : 1,
			\ 'references' : 1,
			\ 'overfull' : 0,
			\ 'underfull' : 0,
			\ 'font' : 1,
			\ 'packages' : {
			\   'default' : 1,
			\   'natbib' : 1,
			\   'biblatex' : 1,
			\   'babel' : 1,
			\   'hyperref' : 1,
			\   'scrreprt' : 1,
			\   'fixltx2e' : 1,
			\   'titlesec' : 0,
			\ },
			\}

