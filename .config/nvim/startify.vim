"This plugin provides a start screen for Vim.
"

Plug 'mhinz/vim-startify' 

" Dont change to the directory of file when opening it.
let g:startify_change_to_dir = 0

" German translation and list order.
let g:startify_list_order = [
			\ ['   Zuletzt verwendet:'],
			\ 'files',
			\ ['   Zuletzt in diesem Ordner verwendet:'],
			\ 'dir',
			\ ['   Sessionen:'],
			\ 'sessions',
			\ ['   Lesezeichen:'],
			\ 'bookmarks',
			\ ['   Befehle:'],
			\ 'commands',
			\ ]

" Bookmarks: 
let g:startify_bookmarks = [
			\ { 'c': '~/.config/nvim/init.vim' },
			\ { 'i': '~/.config/i3/config' },
			\ { 'm': '~/.vim/settings/macros.vim' },
			\ { 'p': '~/.config/polybar/config' },
			\ { 's': '~/.config/nvim/startify.vim' },
			\ { 't': '~/.config/nvim/latex.vim' },
			\ { 'z': '~/.zshrc' },
			\ ]

" Header: no cow, just neovim.
	let g:startify_custom_header = [
	\ '     _   _                 _           ',
	\ '    | \ | | ___  _____   _(_)_ __ ___  ',
	\ '    |  \| |/ _ \/ _ \ \ / / | `_ ` _ \ ',
	\ '    | |\  |  __/ (_) \ V /| | | | | | |',
	\ '    |_| \_|\___|\___/ \_/ |_|_| |_| |_|',
        \ ]

" Unwichtige Dateien: werden bei Startify nicht gezeigt.

let g:startify_skiplist = [
			\ '^/tmp',
			\ '^/home/theo/.vim/bundle',
			\ '^/usr/share/vim/vim80/doc',
			\ '\.vimrc',
			\ '\.vim/settings/macros.vim',
			\ '\.vim/settings/startify.vim',
			\ '\.vim/ftplugin/tex.vim',
			\ '\.vim/plugged'
			\ ]


map <leader>s :Startify<CR>
