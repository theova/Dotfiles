" ===============================================
" Allgemeine Einstellungen
" ===============================================


"""""""""""""""""""""""""""""""""""""""""""""""""
" Backup
set backup		" keep a backup file (restore to previous version)
set backupdir=$HOME/.local/share/nvim/backup " where to store them
set undofile		" keep an undo file (undo changes after closing)
set undodir=$HOME/.local/share/nvim/undo "don't store it in normal folder
set directory=$HOME/.local/share/nvim/swap "where to store the swap files
"""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""
" Miscellaneous:
set encoding=utf-8
set title
autocmd BufWritePre * :%s/\s\+$//e "automatically remove trailing whitespaces
"""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""
" Smartcase: Suche von foo gibt foo, Foo, und foo. Suche von Foo gibt nur Foo,
" aber nicht foo oder FOO. Vim in der Praxis, S. 208
set ignorecase
set smartcase
"""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""""""""""""""""""""
" Clipboard: Always use the system clipboard
set clipboard+=unnamedplus
""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""""""""""""""""""""
" Line numbers:
:set relativenumber
:set number
" :highlight LineNr ctermfg=20 ctermbg=5
" :highlight CursorLineNr ctermfg=21 ctermbg=18
:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END
""""""""""""""""""""""""""""""""""""""""""""""""


""""""""""""""""""""""""""""""""""""""""""""""""
" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
autocmd BufReadPost *
			\ if line("'\"") >= 1 && line("'\"") <= line("$") |
			\   exe "normal! g`\"" |
			\ endif
""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""""""""""""""""""""
" Create file into a new directory
augroup vimrc-auto-mkdir
  autocmd!
  autocmd BufWritePre * call s:auto_mkdir(expand('<afile>:p:h'), v:cmdbang)
  function! s:auto_mkdir(dir, force)
    if !isdirectory(a:dir)
          \   && (a:force
          \       || input("'" . a:dir . "' does not exist. Create? [y/N]") =~? '^y\%[es]$')
      call mkdir(iconv(a:dir, &encoding, &termencoding), 'p')
    endif
  endfunction
augroup END
"""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""
" use jj to exit back to normal mode.
inoremap jj <ESC>
inoremap :w <ESC>:w<CR>
inoremap :wq <ESC>:wq<CR>
inoremap :q <ESC>:q<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""

" write with sudo
command W  execute ":w !sudo tee % > /dev/null"

"""""""""""""""""""""""""""""""""""""""""""""""""
" Verhalten von c-p und c-n wie Pfeiltasten. Vim in der Praxis, S. 77
cnoremap <C-p> <Up>
cnoremap <C-n> <Down>
"""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""
" Einfache Expansion des Verzeichnisses der aktiven Datei
" Vim in der Praxis, S. 108
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'
"""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""
" line numbers
highlight LineNr ctermfg=20 ctermbg=18
highlight CursorLineNr ctermfg=21 ctermbg=18

" mark the 81th character of each line
call matchadd('ColorColumn', '\%81v', 100)
"""""""""""""""""""""""""""""""""""""""""""""""""
