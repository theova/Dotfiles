Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'ervandew/supertab'
Plug 'Shougo/neosnippet.vim'
Plug 'Shougo/neosnippet-snippets'

" Use deoplete.
let g:deoplete#enable_at_startup = 1

" " deoplete tab-complete
inoremap <expr><TAB> pumvisible() ? "\<c-n>" : "\<tab>"

" " Custom snippets
let g:neosnippet#snippets_directory = '~/.vim/templates/snippets'

" " Plugin key-mappings.
" " Note: It must be "imap" and "smap".  It uses <Plug> mappings.
imap <C-space>     <Plug>(neosnippet_expand_or_jump)
smap <C-space>     <Plug>(neosnippet_expand_or_jump)
xmap <C-space>     <Plug>(neosnippet_expand_target)

