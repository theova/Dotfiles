# Neovim
Neovim is a fork of vim, a keyboard centric editor. To write with it feels like "programming your text". Neovim is a powerful tool, but you have to customize it and add some plugins.
To get started, open neovim and write `:Tutor<ENTER>`.

## Plugins
### Plugin manager
* [vim-plug](https://github.com/junegunn/vim-plug): Easy download, update and removing of plugins. Super fast.

### General
* [vim-textobj-user](https://github.com/kana/vim-textobj-user): needed for *vim-textobj-entire*.
* [vim-textobj-entire](https://github.com/kana/vim-textobj-entire): You can select the text of the entire buffer with `ae`.
* [vim-commentary](https://github.com/tpope/vim-commentary): Toggle text to commentary, for all languages.
* [vim-surround](https://github.com/tpope/vim-surround): Surround words, sentences, ... with brackets and other symbols.
* [vim-repeat](https://github.com/tpope/vim-repeat): Repeat the commands above with a point.
* [editorconfig/editorconfig-vim](https:github.com/editorconfig/editorconfig-vim): Set project specific settings.

## Appearence
* [vim-airline](https://github.com/vim-airline/vim-airline): Nice statusline.
* [vim-airline-themes](https://github.com/vim-airline/vim-airline-themes): Themes for the statusline.
* [startify](https://github.com/mhinz/vim-startify): Display recently used files on your startscreen.
* [base16-vim](https://github.com/chriskempson/base16-vim): Use base16 colors.

## Coding, generally
* [Deoplete](https://github.com/Shougo/deoplete.nvim): Completition enginge.
	* [Supertab](https://github.com/ervandew/supertab): Use tab for completion.
	* [neosnippet](https://github.com/Shougo/neosnippet.vim): Snippets functionality.
	* [neosnippet-snippets](https://github.com/Shougo/neosnippet-snippets): Snippets repository.
	* [deoplete-jedi](https://github.com/zchee/deoplete-jedi): Completion for python.
* [ALE](https://github.com/w0rp/ale): Syntax checker. Supports many languages.
* [gutentags](https://github.com/ludovicchabant/vim-gutentags): Automatic generation of tags, they allow you to navigate in your document, e.g. to a function.

## Language specific
* [vimtex](https://github.com/lervag/vimtex): A modern plugin for editing LaTeX files. Simple and lightweight. Supports incremental search.
* [bash-support](https://github.com/vim-scripts/bash-support.vim): "Bash IDE"
* [vim-octave](https://github.com/jvirtanen/vim-octave): Used to write Octave functions/snippets.
